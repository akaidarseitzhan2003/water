import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:magazine/views/dashboard.dart';
import 'package:magazine/views/welcome.dart';

class HttpService {
  static final _client = http.Client();

  static final _loginUrl = Uri.parse('http://uvrcloud.kz/login.php');

  static login(login, password, context) async {
    http.Response response = await _client.post(_loginUrl, body: {
      "login": login,
      "password": password,
    });

    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      var json = jsonDecode(response.body);

      if (json[0] == 'success') {
        await EasyLoading.showSuccess(json[0]);
        await Navigator.push(
            context, MaterialPageRoute(builder: (context) => Dashboard()));
      } else {
        EasyLoading.showError(json[0]);
      }
    } else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode.toString()}");
    }
  }
}

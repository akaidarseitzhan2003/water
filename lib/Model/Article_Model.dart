// ignore_for_file: file_names

class Article {
  int? id;
  String? name;
  String? add_sensor_name;
  int? add_sensor_number;
  String? add_sensor_description;
  double? price;
  String? lastindication;
  String? category;
  String? image;
  String? login;
  String? password;
  DateTime? date;
  int? sum;
  String? description;
  String? model;
  int? high;
  int? gsmnum;
  DateTime? start_work;
  int? last_level;
  int? last_rashod;
  int? last_bat;
  DateTime? last_date;
  DateTime? sensor_date_live;
  Rating? rating;

  Article(
      {this.id,
      this.login,
      this.password,
      this.add_sensor_name,
      this.name,
      this.price,
      this.lastindication,
      this.category,
      this.image,
      this.date,
      this.sum,
      this.description,
      this.model,
      this.high,
      this.gsmnum,
      this.start_work,
      this.last_level,
      this.last_rashod,
      this.last_bat,
      this.last_date,
      this.sensor_date_live,
      this.rating});

  Article.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    login = json['login'];
    password = json['password'];
    name = json['name'];
    price = json['price'] == null ? 0.0 : json['price'].toDouble();
    lastindication = json['lastindication'];
    category = json['category'];
    image = json['image'];
    date = json['date'];
    sum = json['sum'];
    description = json['description'];
    model = json['model'];
    high = json['high'];
    gsmnum = json['gsmnum'];
    start_work = json['start_work'];
    last_level = json['last_level'];
    last_rashod = json['last_rashod'];
    last_bat = json['last_bat'];
    last_date = json['last_date'];
    sensor_date_live = json['sensor_date_live'];
    rating = json['rating'] != null ? Rating.fromJson(json['rating']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['login'] = login;
    data['password'] = password;
    data['name'] = name;
    data['price'] = price;
    data['lastindication'] = lastindication;
    data['category'] = category;
    data['image'] = image;
    data['date'] = date;
    data['sum'] = sum;
    data['description'] = description;
    data['model'] = model;
    data['high'] = high;
    data['gsmnum'] = gsmnum;
    data['start_work'] = start_work;
    data['last_level'] = last_level;
    data['last_rashod'] = last_rashod;
    data['last_bat'] = last_bat;
    data['last_date'] = last_date;
    data['sensor_date_live'] = sensor_date_live;
    if (rating != null) {
      data['rating'] = rating!.toJson();
    }
    return data;
  }
}

class Rating {
  double? rate;
  int? count;

  Rating({this.rate, this.count});

  Rating.fromJson(Map<String, dynamic> json) {
    rate = json['rate'] == null ? 0.0 : json['rate'].toDouble();
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['rate'] = rate;
    data['count'] = count;
    return data;
  }
}

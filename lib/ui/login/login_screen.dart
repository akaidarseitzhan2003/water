import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:magazine/service/http_service.dart';
import 'package:magazine/constants/app_assets.dart';
import 'package:magazine/constants/app_colors.dart';
import 'package:magazine/constants/app_styles.dart';
import 'package:magazine/ui/persons_list/persons_list_screen.dart';
import 'package:magazine/widgets/app_alert_dialog.dart';
import 'package:magazine/widgets/app_button_styles.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

//import 'package:flutter/sv';

import '../../Controler/Api_Controle.dart';
import '../../generated/l10n.dart';
import '../../views/dashboard.dart';
import 'widgets/login_text_field.dart';
import 'widgets/password_text_field.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // late String login;
  // late String password;

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //       appBar: AppBar(title: const Text('Login Page')),
  //       body: Container(
  //         margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
  //         child: Column(
  //           children: [
  //             TextField(
  //               obscureText: false,
  //               decoration: InputDecoration(hintText: 'login'),
  //               onChanged: (value) {
  //                 setState(() {
  //                   login = value;
  //                 });
  //               },
  //             ),
  //             TextField(
  //               obscureText: true,
  //               decoration: InputDecoration(hintText: 'password'),
  //               onChanged: (value) {
  //                 setState(() {
  //                   password = value;
  //                 });
  //               },
  //             ),
  //             InkWell(
  //                 onTap: () async {
  //                   print(password);
  //                   print(login);
  //                   await HttpService.login(login, password, context);
  //                 },
  //                 child: Container(
  //                   margin: const EdgeInsets.symmetric(
  //                       horizontal: 20, vertical: 10),
  //                   child: const Center(
  //                     child: Text(
  //                       "Login",
  //                       style: TextStyle(
  //                           fontWeight: FontWeight.bold, color: Colors.white),
  //                     ),
  //                   ),
  //                   height: 50,
  //                   width: double.infinity,
  //                   decoration: BoxDecoration(
  //                       color: Colors.red,
  //                       borderRadius: BorderRadius.circular(25)),
  //                 ))
  //           ],
  //         ),
  //       )
  //       );
  // }

  final TextEditingController loginController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  void _handleLogin() async {
    final login = loginController.text;
    final password = passwordController.text;

    final response = await http.post(
      Uri.parse('http://uvrcloud.kz/login.php?auth=1'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(<String, String>{
        'login': login,
        'password': password,
      }),
    );

    log('$response');
    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      var json = jsonDecode(response.body);

      if (json[0] == 'success') {
        await EasyLoading.showSuccess(json[0]);
        await Navigator.push(context,
            MaterialPageRoute(builder: (context) => const PersonsListScreen()));
      } else {
        EasyLoading.showError(json[0]);
      }
    } else {
      await EasyLoading.showError(
          "Error Code : ава ${response.statusCode.toString()}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              child: Column(
                children: [
                  TextFormField(
                    controller: loginController,
                    decoration: InputDecoration(labelText: 'Login'),
                  ),
                  TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(labelText: 'Password'),
                  ),
                ],
              ),
            ),
          ),
          ElevatedButton(
            onPressed: _handleLogin,
            child: Text('Login'),
          ),
        ],
      ),
    );
  }
}

//   final formKey = GlobalKey<FormState>();

//   String? login;
//   String? password;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       resizeToAvoidBottomInset: false,
//       body: Column(
//         children: [
//           const Text(
//             "MСД",
//             style: TextStyle(
//               color: Color.fromARGB(217, 77, 103, 195),
//               fontSize: 60,
//               height: 3,
//               fontStyle: FontStyle.italic,
//               fontWeight: (FontWeight.w800),
//             ),
//           ),
//           const Text(
//             "Модуль сбора данных",
//             style: TextStyle(
//               color: Color.fromARGB(217, 77, 103, 195),
//               fontSize: 20,
//               fontStyle: FontStyle.italic,
//               fontWeight: (FontWeight.w800),
//             ),
//           ),
//           const Spacer(),
//           Form(
//             key: formKey,
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Text(
//                   S.of(context).login,
//                   style: AppStyles.s14w400.copyWith(
//                     height: 2.0,
//                     leadingDistribution: TextLeadingDistribution.even,
//                   ),
//                   textAlign: TextAlign.start,
//                 ),
//                 LoginTextField(
//                   onSaved: (login) {
//                     this.login = login;
//                   },
//                 ),
//                 const SizedBox(height: 10.0),
//                 Text(
//                   S.of(context).password,
//                   style: AppStyles.s14w400.copyWith(
//                     height: 2.0,
//                     leadingDistribution: TextLeadingDistribution.even,
//                   ),
//                   textAlign: TextAlign.start,
//                 ),
//                 PasswordTextField(
//                   onSaved: (value) {
//                     password = value;
//                   },
//                 ),
//                 const SizedBox(height: 24.0),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   children: [
//                     Expanded(
//                       child: ElevatedButton(
//                         child: Text(S.of(context).signIn),
//                         onPressed: () async {
//                           final isValidated =
//                               formKey.currentState?.validate() ?? false;
//                           if (isValidated) {
//                             FocusScope.of(context).unfocus();
//                             formKey.currentState?.save();
//                               log("dsss");
//                             if (await authenticateUser(login!, password!)) {
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                   builder: (context) =>
//                                       const PersonsListScreen(),
//                                 ),
//                               );
//                             } else {
//                               showDialog(
//                                 context: context,
//                                 builder: (context) {
//                                   return AppAlertDialog(
//                                     title: Text(S.of(context).error),
//                                     content: Text(
//                                       S.of(context).wrongLoginOrPassword,
//                                     ),
//                                   );
//                                 },
//                               );
//                             }
//                           }
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

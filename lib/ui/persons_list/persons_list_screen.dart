// ignore_for_file: non_constant_identifier_names

import 'package:magazine/Controler/Api_Controle.dart';
import 'package:magazine/Model/Article_Model.dart';
import 'package:magazine/View/Compenant/Categorie_Item.dart';
import 'package:magazine/View/Screens/Article_Screen.dart';
import 'package:flutter/material.dart';

import '../../widgets/app_nav_bar.dart';

class PersonsListScreen extends StatefulWidget {
  const PersonsListScreen({Key? key}) : super(key: key);

  @override
  State<PersonsListScreen> createState() => _PersonsListScreenState();
}

class _PersonsListScreenState extends State<PersonsListScreen> {
  int _value = 0;

  // List<String> Categorietitle = [
  //   "Датчики",
  //   "Загрузка",
  //   "Журнал",
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const AppNavBar(current: 0),
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(200, 16, 43, 138),
        elevation: 0,
        title: const Text(
          'MSD',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 5,
          ),
          const SizedBox(
            height: 20,
          ),
          const SizedBox(
            height: 20,
          ),
          FutureBuilder<List<Article>>(
            future: _value == 0
                ? ApiControl.fetchArticle()
                : ApiControl.fetchArticleByCategorie([_value].toString()),
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: GridView.builder(
                          physics: const BouncingScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data!.length,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20,
                            crossAxisCount: 2,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: (() {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => ArticlScreen(
                                      articalID: snapshot.data![index].id!,
                                    ),
                                  ),
                                );
                              }),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  snapshot.data![index].image!),
                                              fit: BoxFit.contain)),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    snapshot.data![index].name!,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '\$' +
                                        snapshot.data![index].price.toString(),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: const TextStyle(
                                      color: Color.fromARGB(255, 253, 104, 104),
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  : const Expanded(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }
}

// ignore_for_file: avoid_print, non_constant_identifier_names, file_names

import 'package:magazine/Model/Article_Model.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<bool> authenticateUser(String login, String password) async {
  final response = await http.post(
    Uri.parse('http://uvrcloud.kz/login.php?auth=1'),
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(<String, String>{
      'login': login,
      'password': password,
    }),
  );

  if (response.statusCode == 200) {
    final jsonResponse = json.decode(response.body);
    final isSuccess = jsonResponse['success'];
    return isSuccess ?? false;
  } else {
    throw Exception('Failed to authenticate user.');
  }
}

class ApiControl {
  var dio = Dio();

  static Future<List<Article>> fetchArticle() async {
    Response response =
        await Dio().get('http://uvrcloud.kz/?page=sensors_table');
    return (response.data as List).map((x) => Article.fromJson(x)).toList();
  }

  static Future<List<Article>> fetchArticleByCategorie(
      String categoriename) async {
    Response response = await Dio()
        .get('http://uvrcloud.kz/?f=json&page=sensors_table&start=0');
    return (response.data as List).map((x) => Article.fromJson(x)).toList();
  }

  static Future<Article> fetchArticleByID(int aricle_ID) async {
    Response response =
        await Dio().get('http://uvrcloud.kz/?page=users_table&f=json&start=0');
    Article _article = Article.fromJson(response.data);
    return _article;
  }
}
